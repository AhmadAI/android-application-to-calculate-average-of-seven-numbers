package com.example.q6;

import static com.example.q6.R.id.button;

import android.annotation.SuppressLint;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.core.view.WindowCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.q6.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button;
        EditText etx1, etx2, etx3, etx4, etx5, etx6, etx7;
        TextView rtv;

        etx1=findViewById(R.id.etxt1);
        etx2=findViewById(R.id.etxt2);
        etx3=findViewById(R.id.etxt3);
        etx4=findViewById(R.id.etxt4);
        etx5=findViewById(R.id.etxt5);
        etx6=findViewById(R.id.etxt6);
        etx7=findViewById(R.id.etxt7);
        rtv=findViewById(R.id.rtv);
        button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                int num1=Integer.parseInt(etx1.getText().toString());
                int num2=Integer.parseInt(etx2.getText().toString());
                int num3=Integer.parseInt(etx3.getText().toString());
                int num4=Integer.parseInt(etx4.getText().toString());
                int num5=Integer.parseInt(etx5.getText().toString());
                int num6=Integer.parseInt(etx6.getText().toString());
                int num7=Integer.parseInt(etx7.getText().toString());

                int average = (num1 + num2 + num3 + num4 + num5 + num6 + num7) / 7;

                rtv.setText("The average of the above seven numbers is: " +average);

            }
        });


    }
}